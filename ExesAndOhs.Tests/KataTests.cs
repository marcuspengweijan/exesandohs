﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExesAndOhs.Tests
{
    [TestClass]
    public class KataTests
    {
        [DataTestMethod]
        [DataRow("xo", true, DisplayName = "xo_true")]
        [DataRow("xxOo", true, DisplayName = "xxOo_true")]
        [DataRow("xxxm", false, DisplayName = "xxxm_false")]
        [DataRow("Oo", false, DisplayName = "Oo_false")]
        [DataRow("ooom", false, DisplayName = "ooom_false")]
        [DataRow("", true, DisplayName = "empty_true")]
        public void XOTest(string input, bool expected)
        {
            Assert.AreEqual(expected, Kata.XO(input));
        }
    }
}
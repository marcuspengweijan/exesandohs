﻿using System.Linq;

namespace ExesAndOhs
{
    public class Kata
    {
        public static bool XO(string input)
        {
            return input.ToLower().Count(x => x.Equals('x')).Equals(input.ToLower().Count(x => x.Equals('o')));
        }
    }
}
